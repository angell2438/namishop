import { combineReducers } from "redux";
import auth from './authReducer';
import catalog from './catalogReducer';
import cafes from './categoryReducer';
import category from './categoryReducer';

export default combineReducers({
  auth, catalog, cafes, category
});