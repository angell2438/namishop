export const cafesTypes ={
    LoaderStart: 'cafes_loader_start',
    LoaderEnd: 'cafes_loader_end',
    ListReceive: 'cafes_list_receive',
    ItemReceive: 'cafes_item_receive',
}
export const catalogTypes ={
    LoaderStart: 'catalog_loader_start',
    LoaderEnd: 'catalog_loader_end',
    ListReceive: 'catalog_list_receive',
    ItemReceive: 'catalog_item_receive',
}