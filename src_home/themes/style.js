import {StyleSheet} from 'react-native';
import theme from './theme';

const styles = StyleSheet.create({
    title: {
        color: theme.colorTxt,
        fontSize: theme.fontSize + 2,
        fontWeight: '500'
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#e6e6e6',
        fontSize: 14
    },
    header: {
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: "#e6e6e6"
    },
    slide: {
        backgroundColor: '#f1f1f1',
        flex: 1
    },
    image: {
        flex: 1,
        width: '100%'
    }
});

export default styles;
