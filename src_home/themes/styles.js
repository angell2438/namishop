import {StyleSheet, Dimensions} from "react-native";
var {height, width} = Dimensions.get('window');
const defaultFontSize = 10;

export const generalStyle = StyleSheet.create({
    loading:{
        marginTop:20,
        alignSelf:'center',
        fontSize:18,
        color:"#fff"
    },
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    bgColor:{
        backgroundColor: "#fff"
    },
    content: {
        flex: 1,
        backgroundColor: "#fff",
        // height: 270
    },
    button: {
        borderRadius: 5,
        backgroundColor: "#e83c4d",
        padding: 15,
        width: '100%'
    },
    linkInside:{
        color: "#1a1a1a",
        fontSize: 15,
    },
    miniButtons:{
        borderRadius: 5,
        backgroundColor: "#fff",
        padding: 10,
        marginLeft: 30,
        marginRight: 30
    },
    buttonInside: {
        alignSelf: 'center',
        color: '#fff',
        fontWeight:'500',
        fontSize: 1.5* defaultFontSize
    },
    tabContainer:{
        backgroundColor: '#fff',
        marginTop:30,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 30,
        width: width
    },
});

export const cafeItemStyle = StyleSheet.create({
    mainImage:{
        width:"100%",
        height:250
    },
    title:{
        color:"#ecf0f1",
        fontSize:20,
        textAlign: 'center',
        flex: 1,
    },
    titleLine:{
        padding:5,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    rating:{
        flexDirection:'row'
    }

});
export const cafeListStyle = StyleSheet.create({
    cafeWrap:{
        margin: 5,
        padding:5,
        backgroundColor:"#fff",
        shadowColor: "#333",
        shadowOffset: {width: 0,height: 3},
        shadowOpacity: 0.6,
        flexDirection:'row'
    },
    imageWrap:{
        paddingRight:5
    },
    image:{
        width:100,
        height:100
    },
    title:{
        fontSize:18
    }
});
export const headerStyle = StyleSheet.create({
    buttonHeaderNav: {
        backgroundColor: "#f8f8f8",
        padding: 5
    },
    header:{
        width: width,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:"center",
        marginBottom: 5,
        paddingTop: 14
    },
    headerText:{
        fontSize: 20
    },
    headerSearch:{
        width: width,
        borderRadius: 5,
        marginBottom: 15,
    },
    inputIconHeader:{
        marginLeft:20,
        marginRight:20,
        marginTop: 10,
        color: "#1a1a1a"
    },
    navMenuBtn:{
        color: "#1a1a1a",
        fontSize: 30,
    },
    title: {
        color: "#1a1a1a",
        alignItems: 'center',
        justifyContent: 'center',
    },
    top: {
        backgroundColor: "#f8f8f8",
        borderBottomWidth: 0,
        // darkenHeader: "red"
    },
});
export const loginStyle = StyleSheet.create({
    titleSoc: {
        color: "#1a1a1a",
        alignItems: 'center',
        justifyContent: 'center',
        fontSize:14,
        marginTop: 30,
        marginBottom: 20,
        flex: 1,
        textAlign: 'center'
    },
    loginText: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 29
    },
    navigation: {
        backgroundColor: "#f8f8f8",
        height: 88,
        justifyContent:'center'
        // marginBottom: 20
    },
    loginBnt: {
        backgroundColor: "#fff",
        justifyContent:'center',
        marginBottom: 30,
        marginTop: 30
    },
    inputIcon:{
        // marginLeft:20,
        marginRight:15,
        marginTop: 10,
        color: "#e83c4d",
        borderWidth:0,
    },
    inputWrapper:{
        height: 40,
        // width: "100%",
        // borderRadius: 5,
        backgroundColor: "#fff",
        // marginLeft: 15,
        // marginRight: 15,
        marginBottom:15,
        flexDirection:'row',
        borderWidth:0,
        borderBottomWidth: 0
    },
    inputStyle:{
        height: 40,
        borderWidth: 0,
        color: "#808080",
        borderBottomWidth:1,
        borderBottomColor: "#e6e6e6"
    },
    titleTab:{
        fontSize: 20,
        color: "#1a1a1a",
        textAlign: "center",
        marginBottom: 30
    },
    containerButton:{
        padding: 25
    },
});
export const categoryStyle = StyleSheet.create({
    iconArrow:{
        color: "#808080",
    },
});
export const productStyle = StyleSheet.create({
    productContainer:{
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems:"center",
        borderWidth:1,
        borderTopColor: "#e6e6e6",
        borderBottomColor: "#e6e6e6",
        marginBottom: 10
    },
    productName:{
        fontSize: 14,
        color: "#1a1a1a",
        justifyContent: "center"
    },
    titleStyle:{
        fontSize: 14,
        paddingBottom:10,
        paddingTop: 15
    },
    item:{
        width: '50%',
        flexDirection:'column',
        alignItems:"center",
        padding: 15,
        borderRightWidth: 1,
        borderRightColor: "#e6e6e6"
    },
    price:{
        fontSize: 16,
        color: "#1a1a1a"
    },
    h2container:{
        width: width,
        padding: 10
    },
    blockName:{
        fontSize: 14,
        color: "#1a1a1a"
    },
    priceOld:{
        fontSize: 12,
        color: "#808080",
    },
    itemPrice:{
        flexDirection:'row',
        alignItems:"center",
        justifyContent: 'space-between',
        paddingLeft:10,
        paddingRight:10
    },
    itemPriceCol:{
        flexDirection:'column',
        alignItems:"center"
    },
});
export const bannerStyle = StyleSheet.create({
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    imageBanner: {
        width: width,
        height: 120,
        flex: 1
    },
});
// export const styles = StyleSheet.create({});
//
// export default styles;


