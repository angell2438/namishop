import { createIconSetFromFontello } from 'react-native-vector-icons';

import fontelloConfig from "../"
export const  IconMoon = createIconSetFromFontello(fontelloConfig);
