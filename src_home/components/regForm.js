import React from 'react';
import {
    H1,
    Content,
    Item,
    Icon,
    Input,
    Button,
    Text,
    ListItem,
    Body,
    CheckBox, Spinner, View, Container, Toast

} from 'native-base';
import {connect} from 'react-redux';

import theme from '../themes/theme';
import styles from '../themes/style';
import {signUp, changeAuthData} from '../actions/myActions';
import {Actions} from 'react-native-router-flux';

class RegForm extends React.Component {

    constructor () {
        super();
        this.state = {
            checked: false,
            loading: false
        }
    }

    check() {
        this.setState({
            checked: !this.state.checked
        })
    }

    registration () {
        this.setState({loading: true});

        this.props.signUp(this.props.auth)
            .then(Actions.main)
            .catch(error => alert(error))
            .finally(()=> {
                this.setState({loading: false});
            });

        console.log(this.props.auth)
    }

    renderForm () {
        return (
            <Content padder>
                <H1 style={{textAlign: 'center', marginTop: 30, marginBottom: 20}}>Регистрация</H1>
                <Item style={{
                    borderBottomWidth: 0
                }}>
                    <Icon active name='ios-person-outline'
                          style={{
                              color: theme.red,
                              fontSize: 31,
                              width: 35
                          }}
                    />
                    <Input placeholder='Имя и фамилия'
                           placeholderTextColor={theme.colorTxtDimmed}
                           style={styles.input}
                           autoCapitalize="words"
                           onChangeText={(name) => this.props.changeAuthData('name', name)}
                           value={this.props.auth.name}
                    />
                </Item>
                <Item style={{
                    borderBottomWidth: 0
                }}>
                    <Icon active name='ios-mail-outline'
                          style={{
                              color: theme.red,
                              fontSize: 31,
                              width: 35
                          }}
                    />
                    <Input placeholder='Введите телефон или email'
                           placeholderTextColor={theme.colorTxtDimmed}
                           style={styles.input}
                           onChangeText={(email) => this.props.changeAuthData('email', email)}
                           value={this.props.auth.email}
                           keyboardType='email-address'
                           autoCapitalize="none"
                    />
                </Item>
                <Item style={{
                    borderBottomWidth: 0
                }}>
                    <Icon active name='ios-lock-outline'
                          style={{
                              color: theme.red,
                              fontSize: 31,
                              width: 35
                          }}
                    />
                    <Input placeholder='Пароль'
                           placeholderTextColor={theme.colorTxtDimmed}
                           style={styles.input}
                           secureTextEntry={!this.state.checked}
                           onChangeText={(password) => this.props.changeAuthData('password', password) }
                           value={this.props.auth.password}
                    />
                </Item>
                <ListItem style={{
                    borderBottomWidth: 0,
                    marginLeft: 0,
                    paddingRight: 0
                }}>
                    <CheckBox color={theme.red}
                              checked={this.state.checked}
                              onPress={this.check.bind(this)}
                    />
                    <Body>
                    <Text style={{
                        color: theme.colorTxt,
                        fontSize: theme.fontSize
                    }}>
                        Показать пароль
                    </Text>
                    </Body>
                </ListItem>

                <Text style={{
                    fontSize: 12,
                    color: theme.colorTxtDimmed,
                    textAlign: 'center',
                    width: '80%',
                    alignSelf: 'center',
                    lineHeight: 13,
                    marginTop: 5
                }}>
                    Пароль должен быть от 6 до 16 символов,
                    содержать цифры и заглавные буквы и не должен совпадать с именем и email
                </Text>

                <Button block danger title="Зарегистрироваться" style={{
                    backgroundColor: theme.red,
                    borderRadius: 5,
                    marginTop: 30,
                    marginBottom: 15
                }}
                        onPress={this.registration.bind(this)}
                >
                    <Text uppercase={false} style={{fontSize: theme.fontSize + 2}}>Зарегистрироваться</Text>
                </Button>


                <Text style={{
                    color: theme.colorTxt,
                    fontSize: theme.fontSize + 1,
                    textAlign: 'center'
                }}>
                    Регистрируясь, вы соглашаетесь {"\n"}
                    <Text style={{color: "#3a96ff"}}>с пользовательским соглашением</Text>
                </Text>
            </Content>
        )
    }


    render() {
        return (
            <Container style={{ justifyContent: 'center'}}>
                {
                    this.state.loading ? <Spinner color={theme.red}/> : this.renderForm()
                }
            </Container>
        );
    }
}

const mapStateToProps = ({auth}) => {
    return { auth }
};

const mapActionsToProps = {
    changeAuthData,
    signUp
};

export default  connect(mapStateToProps, mapActionsToProps )(RegForm);
