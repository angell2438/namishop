import {Content, Item, Input, Icon, Radio, Button, Text, ListItem, Right} from 'native-base';
import Image from 'react-native-remote-svg';
import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import { loginStyle, generalStyle } from "../themes/styles";
import Loader from "../components/loader";
import ButtonItem from "../components/button_item";
import { signIn, changeAuthData } from "../actions";

class tabSigIn extends Component {
    constructor() {
        super();
        this.state = {
            loading: false,
        }
    }
    componentWillMount() {
        console.log('props', this.props);
    }

    changeAuthData(field, value) {
        this.props.changeAuthData(field, value);
    }

    loginButtonPress() {
        this.setState({loading: true});

        this.props.signIn(this.props.auth)
            .then(Actions.welcome)
            .catch(error => {
                alert(error)
            })
            .finally(() => {
                this.setState({loading: false})
            });
    }

    renderForm() {
        return (
            <Content>
                <Text style={loginStyle.titleTab}>
                    Вход
                </Text>
                <Item style={loginStyle.inputWrapper}>
                    <Icon name="person" style={loginStyle.inputIcon}/>
                    <Input
                        onChangeText={(email) => this.changeAuthData('email',email)}
                        placeholder="Е-mail"
                        value={this.props.auth.email}
                        style={loginStyle.inputStyle}
                        underlineColorAndroid={'transparent'}
                    />
                </Item>
                <Item style={loginStyle.inputWrapper}>
                    <Icon name="ios-unlock" style={loginStyle.inputIcon}/>
                    <Input
                        onChangeText={(password) => this.changeAuthData('password',password)}
                        value={this.props.auth.password}
                        placeholder="Пароль"
                        style={loginStyle.inputStyle}
                        underlineColorAndroid={'transparent'}
                           secureTextEntry
                    />
                </Item>
                <Item style={loginStyle.inputWrapper}>
                    <ListItem>
                        <Radio selected={false}/>
                        <Text>Показать пароль</Text>
                    </ListItem>
                    <Right>
                        <Button transparent>
                            <Text>Забыли пароль?</Text>
                        </Button>
                    </Right>
                </Item>
                <ButtonItem text="Вход" style={generalStyle.button} click={this.loginButtonPress.bind(this)}/>
                <Text style={loginStyle.titleSoc}>Войти через соцсети</Text>
                <Item style={loginStyle.containerButton}>
                    <Button transparent>
                        <Image source={require('../themes/svg/facebook-outlined-logo.svg')}/>
                        <Text>faceboock</Text>
                    </Button>
                    <Button transparent>
                        <Image source={require('../themes/svg/google-plus-1.svg')}/>
                        <Text>Google</Text>
                    </Button>
                </Item>
            </Content>
        )
    }

    render() {
        const {props} = this;
        return (
            <Content style={generalStyle.tabContainer}>
                {this.state.loading ? <Loader/> : this.renderForm()}
            </Content>

        )
    }
}


export default connect(({auth})=>{ return {auth}}, { changeAuthData, signIn })(tabSigIn);


