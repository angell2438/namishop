import React from 'react';
import {
    Body,
    Button,
    CheckBox,
    Col,
    Container,
    Content,
    Grid,
    H1,
    Icon,
    Input,
    Item,
    ListItem,
    Row,
    Spinner,
    Text
} from 'native-base';

import {connect} from 'react-redux';

import Image from 'react-native-remote-svg';
import {Actions} from 'react-native-router-flux';
import {signIn, changeAuthData} from '../actions/myActions';


import theme from '../themes/theme';
import styles from '../themes/style';

class LoginForm extends React.Component {

    constructor () {
        super();

        this.state = {
            checked: false,
            email: '',
            password: '',
            loading: false
        }
    }

    check() {
        this.setState({
            checked: !this.state.checked
        })
    }

    logIn () {
        this.setState({loading: true});

        signIn(this.state)
            .then(Actions.main)
            .catch(error => alert(error))
            .finally(()=> {
                this.setState({loading: false});
            })
    }

    renderForm () {
        return (
            <Content padder>

                <H1 style={{textAlign: 'center', marginTop: 30, marginBottom: 20}}>Вход</H1>
                <Item style={{
                    borderBottomWidth: 0
                }}>
                    <Icon active name='ios-mail-outline'
                          style={{
                              color: theme.red,
                              fontSize: 31,
                              width: 35
                          }}
                    />
                    <Input placeholder='Введите телефон или email'
                           placeholderTextColor={theme.colorTxtDimmed}
                           style={styles.input}
                           underlineColorAndroid='transparent'
                           onChangeText={ (email) => {this.setState({email})} }
                           value={this.state.email}
                           keyboardType="email-address"
                           autoCapitalize="none"
                    />
                </Item>
                <Item style={{
                    borderBottomWidth: 0,
                }}>
                    <Icon active name='ios-lock-outline'
                          style={{
                              color: theme.red,
                              fontSize: 31,
                              width: 35
                          }}
                    />
                    <Input placeholder='Пароль'
                           placeholderTextColor={ theme.colorTxtDimmed }
                           style={ styles.input }
                           secureTextEntry={ !this.state.checked }
                           underlineColorAndroid='transparent'
                           onChangeText={(password)=> { this.setState({password}) }}
                           value={this.state.password}
                    />

                </Item>

                <Grid>
                    <Row style={{
                        alignItems: 'center'
                    }}>
                        <Col>
                            <ListItem style={{
                                borderBottomWidth: 0,
                                marginLeft: 0,
                                paddingRight: 0
                            }}>
                                <CheckBox color={theme.red}
                                          checked={ this.state.checked }
                                          onPress={ this.check.bind(this) }
                                />
                                <Body>
                                <Text style={{
                                    color: theme.colorTxt,
                                    fontSize: theme.fontSize
                                }}>
                                    Показать пароль
                                </Text>
                                </Body>
                            </ListItem>
                        </Col>
                        <Col>
                            <Button transparent style={{
                                marginLeft: 'auto'
                            }}>
                                <Text uppercase={false}
                                      style={{ color: theme.red, textAlign: 'right', fontSize: theme.fontSize }}>
                                    Забыли пароль?
                                </Text>
                            </Button>
                        </Col>
                    </Row>
                </Grid>

                <Button block danger title="Войти" style={{
                    backgroundColor: theme.red,
                    borderRadius: 5,
                    marginTop: 30,
                    marginBottom: 30
                }} onPress={ this.logIn.bind(this) }>
                    <Text uppercase={false} style={{fontSize: theme.fontSize + 2}}>Войти</Text>
                </Button>

                <Row>
                    <Text style={{
                        color: theme.colorTxt,
                        fontSize: theme.fontSize,
                        textAlign: 'center',
                        flex: 1
                    }}>Войти через соцсети</Text>
                </Row>
                <Row style={{justifyContent: 'space-evenly',marginTop: 10}}>

                    <Button title="facebook" transparent iconLeft>
                        <Image
                            style={{
                                width: 17,
                                height: 30
                            }}
                            source={require('../img/fb.svg')}
                        />
                        <Text uppercase={false} style={{fontSize: theme.fontSize + 2}}>Facebook</Text>
                    </Button>

                    <Button title="Google Plus" transparent iconLeft>
                        <Image
                            style={{
                                width: 41,
                                height: 27
                            }}
                            source={require('../img/google-plus.svg')}
                        />
                        <Text style={{color: theme.red, fontSize: theme.fontSize + 2}} uppercase={false}>Google+</Text>
                    </Button>

                </Row>
            </Content>
        )
    }

    render() {
        return (

            <Container style={{ justifyContent: 'center' }}>
                {
                    this.state.loading ? <Spinner color={theme.red} /> : this.renderForm()
                }
            </Container>
        )
    }
}

// export default LoginForm;
export default connect(({auth}) => {return auth})(LoginForm);