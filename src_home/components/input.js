import { Item, Input, Icon } from 'native-base';
import React, {Component} from 'react';
import { loginStyle } from "../themes/styles";

class TextInput extends Component {
    render() {
        const {props} = this;
        return (
            <Item>
                 <Icon active name={props.icon} />
                <Input placeholder={props.placeholder} style={ loginStyle.inputStyle } underlineColorAndroid={'transparent'}/>
            </Item>
        )
    }
}
export default TextInput;