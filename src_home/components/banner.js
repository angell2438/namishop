import React, { Component } from 'react'
import {
    Text,
    View,
    Image,
    Dimensions
} from 'react-native';
import Swiper from 'react-native-swiper';
import { generalStyle, bannerStyle } from "../themes/styles";
const { width } = Dimensions.get('window');
export default class extends Component {
    render () {
        return (
            <View style={generalStyle.container}>
                <Swiper style={bannerStyle.wrapper} height={120} horizontal={true} autoplay>
                    <View style={bannerStyle.slide} title={<Text numberOfLines={1}>Aussie tourist dies at Bali hotel</Text>}>
                        <Image resizeMode='stretch' style={bannerStyle.imageBanner} source={require('../themes/banner.png')} />
                    </View>
                    <View style={bannerStyle.slide} title={<Text numberOfLines={1}>Big lie behind Nine’s new show</Text>}>
                        <Image resizeMode='stretch' style={bannerStyle.imageBanner} source={require('../themes/banner.png')} />
                    </View>
                    <View style={bannerStyle.slide} title={<Text numberOfLines={1}>Why Stone split from Garfield</Text>}>
                        <Image resizeMode='stretch' style={bannerStyle.imageBanner} source={require('../themes/banner.png')} />
                    </View>
                    <View style={bannerStyle.slide} title={<Text numberOfLines={1}>Learn from Kim K to land that job</Text>}>
                        <Image resizeMode='stretch' style={bannerStyle.imageBanner} source={require('../themes/banner.png')} />
                    </View>
                </Swiper>
            </View>
        )
    }
}
