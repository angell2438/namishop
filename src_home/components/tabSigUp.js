import { Content, Item, Input, Icon, Text, Button, ListItem, Radio } from 'native-base';
// import Image from 'react-native-remote-svg';
import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import { loginStyle, generalStyle } from "../themes/styles";
import Loader from "../components/loader";
import ButtonItem from "../components/button_item";
import { signUp } from "../actions";
class tabSigUp extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            password: '',
            loading: false,
        }
    }

    loginButtonPress() {
        this.setState({loading: true});

        signUp(this.state)
            .then(Actions.welcome)
            .catch(error => {
                alert(error)
            })
            .finally(() => {
                this.setState({loading: false})
            });
        // console.warn('12');
    }
    // console.warn(signUp);
    renderForm() {
        return (
            <Content>
                <Text style={loginStyle.titleTab} >
                    Регистрация
                </Text>
                <Item style={loginStyle.inputWrapper}>
                    <Icon name="person" style={loginStyle.inputIcon}/>
                    <Input
                        onChangeText={(name) => this.setState({name})}
                        placeholder="Имя и фамилия"
                        value={this.state.name}
                        style={loginStyle.inputStyle}
                        underlineColorAndroid={'transparent'}
                    />
                </Item>
                <Item style={loginStyle.inputWrapper}>
                    <Icon name="ios-mail" style={loginStyle.inputIcon}/>
                    <Input
                        onChangeText={(email) => this.setState({email})}
                        placeholder="E-mail"
                        value={this.state.email}
                        style={loginStyle.inputStyle}
                        underlineColorAndroid={'transparent'}
                    />
                </Item>
                <Item style={loginStyle.inputWrapper}>
                    <Icon name="ios-unlock" style={loginStyle.inputIcon}/>
                    <Input
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                        placeholder="Пароль"
                        style={loginStyle.inputStyle}
                        underlineColorAndroid={'transparent'}
                        secureTextEntry
                    />
                </Item>
                <Item style={loginStyle.inputWrapper}>
                    <ListItem>
                        <Radio selected={false} />
                        <Text>Показать пароль</Text>
                    </ListItem>
                </Item>
                <Text style={loginStyle.titleSoc}>
                    Пароль должен быть от 6 до 16 символов,
                    содержать цифры и заглавные буквы и не должен совпадать с именем и email
                </Text>
                <ButtonItem text="Реєстрація" style={generalStyle.button} click={this.loginButtonPress.bind(this)}/>
                <Text style={loginStyle.titleSoc}>Регистрируясь, вы соглашаетесь</Text>
                <Button transparent>
                    <Text>с пользовательским соглашением</Text>
                </Button>
            </Content>
        )
    }
    render() {
        const {props} = this;
        return (
            <Content style={generalStyle.tabContainer}>
                {this.state.loading ? <Loader/> : this.renderForm()}
            </Content>
        )
    }
}
export default tabSigUp;