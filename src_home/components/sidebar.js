import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Alert, ImageBackground } from 'react-native';
import { Container, Thumbnail, Content, Icon, CardItem, Item, Card, Text, Header, Drawer, Button, ListItem, Left, Body, Right } from 'native-base';

const currentUser = 'test user';

const submenu = (<View>
    <ListItem icon onPress={() => alert('Person 1')}>
        <Left><Icon name="person" /></Left>
        <Body><Text>Person 1</Text></Body>
    </ListItem>
    <ListItem icon onPress={() => alert('Person 2')}>
        <Left><Icon name="person" /></Left>
        <Body><Text>Person 2</Text></Body>
    </ListItem>
    <ListItem icon onPress={() => alert('Person 3')}>
        <Left><Icon name="person" /></Left>
        <Body><Text>Person 3</Text></Body>
    </ListItem>
</View>)

export default class SideNavigationBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawer: false,
            children: props.children,
            isMenuActive: props.isMenuActive,
            onSideMenuChange: props.onSideMenuChange
        };
    }

    render() {
        let icon = 'ios-add-outline';
        if (this.state.expanded) {
            icon = 'ios-remove-outline';
        }

        return (
            <Drawer
                style={{ border: 0 }}
                ref={(ref) => { drawer = ref; }}
                open={this.state.drawer}
                content={<MenuComponent currentUser={currentUser} />}>
                {/* {this.state.children} */}
                <Text style={{ flex: 1, padding: 20 }} onPress={() => this.setState({ drawer: !this.state.drawer })}>Main Content</Text>
            </Drawer >
        );
    }
}

class MenuComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = { item1: false, item2: false, item3: false };
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#fff', paddingTop: 20 }}>
                <ListItem icon onPress={() => this.setState({ item1: !this.state.item1 })}>
                    <Left><Icon name="plane" /></Left>
                    <Body><Text>Item 1</Text></Body>
                    <Right><Icon name="md-arrow-forward" /></Right>
                </ListItem>

                {this.state.item1 ? submenu : null}

                <ListItem icon onPress={() => this.setState({ item2: !this.state.item2 })}>
                    <Left><Icon name="wifi" /></Left>
                    <Body><Text>Item 2</Text></Body>
                    <Right><Icon name="md-arrow-forward" /></Right>
                </ListItem>

                {this.state.item2 ? submenu : null}

                <ListItem icon onPress={() => this.setState({ item3: !this.state.item3 })}>
                    <Left><Icon name="bluetooth" /></Left>
                    <Body><Text>Item 3</Text></Body>
                    <Right><Icon name="md-arrow-forward" /></Right>
                </ListItem>

                {this.state.item3 ? submenu : null}
                <Text style={{ padding: 20 }}>Current user: {this.props.currentUser}</Text>
            </Container>
        )
    }
}