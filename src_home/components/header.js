import React, { Component } from 'react';
import { Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import Image from 'react-native-remote-svg';
import { headerStyle } from "../themes/styles";
const HeaderBar = (props)=>(
                <Header style={headerStyle.top}>
                    <Left>
                        <Button transparent onPress={() => this.setState({ drawer: !this.state.drawer })}>
                            <Icon style={headerStyle.navMenuBtn} name='menu' />
                        </Button>
                    </Left>
                    <Body>
                    <Title  style={headerStyle.title}>{ props.title || 'Awesome project'  } </Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Image source={require('../themes/svg/shopping-cart-3.svg')} />
                        </Button>
                    </Right>
                </Header>
        );
export default HeaderBar;
