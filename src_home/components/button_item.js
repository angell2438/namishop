import { Text, Button } from 'native-base';
import React from 'react';
import { generalStyle } from "../themes/styles";

const ButtonItem = (props)=>(
    <Button block onPress={ props.click } style={ generalStyle.button }>
        <Text style={ generalStyle.buttonInside }>{ props.text } </Text>
    </Button>
);

export default ButtonItem;