import { ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';
import React from 'react';
import { generalStyle } from "../themes/styles";

const LinkBtn = (props) =>(
    <ListItem icon onPress={ props.click }>
        <Left>
            <Icon name="plane" color="#000"/>
        </Left>
        <Body>
             <Text style={ generalStyle.linkInside }>{props.text}</Text>
        </Body>
    </ListItem>
)
export default LinkBtn;