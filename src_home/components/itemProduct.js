import { View, Text, Image } from 'react-native';
import React from 'react';
import { productStyle } from "../themes/styles";

const Product =(props)=>(

        <View style= { productStyle.item } >
            <Image style={ productStyle.logoProd }
            source={require('../themes/prod1.png') }
            />
            <Text style={ productStyle.productName } >
                {props.title}
            </Text>
            <View style= { productStyle.itemPrice } >
                <View  style= { productStyle.itemPriceCol } >
                    <Text style={productStyle.priceOld}>
                        {props.priceold}
                    </Text>
                    <Text style={productStyle.price}>
                        {props.price}
                    </Text>
                </View>
                {/*<TouchableOpacity style={ styles.more } onPress={this.onPress}>*/}
                    {/*<Icon name="ios-menu" size={20} color="#000" style={styles.inputIcon}/>*/}
                {/*</TouchableOpacity>*/}
            </View>
        </View>
)
export default Product;