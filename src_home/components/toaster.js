import React, {Component} from 'react';
import { View, Text } from 'react-native';
// import { loginStyle, generalStyle } from "../themes/styles";
class Toaster {
    constructor(){
        this.message="";
        this.showlistener= null;
        this.hidelistener= null;
    }
    registerShowListener(listener){
        this.showlistener= listener;
    }
    registerHideListener(listener){
        this.hidelistener= listener;
    }
    showMessage(message){
        this.message= message;
        this.showlistener(message);
        if(this.showlistener && this.hidelistener){
            this.showlistener(message);
            setTimeout(()=>{
                this.hidelistener()
            }, 5000)
        }
    }
}

export const ToasterView =(props)=>
    <View style={{position:'absolute', backgroundColor:'red', top:30, width:'100%'}}>
        <Text>{props.text}</Text>
    </View>


export default  new Toaster();
// import React, { Component } from "react";
// import { Container, Header, Title, Content, Text, Button, Icon, Left, Right, Body, Toast } from "native-base";
// class ToastButton extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             showToast: false
//         };
//     }
//     render() {
//         return (
//             <Container>
//                 <Header>
//                     <Left>
//                         <Button transparent>
//                             <Icon name="arrow-back" />
//                         </Button>
//                     </Left>
//                     <Body>
//                     <Title>Toast Button Style</Title>
//                     </Body>
//                     <Right />
//                 </Header>
//                 <Content padder>
//                     <Button
//                         onPress={() =>
//                             Toast.show({
//                                 text: "Wrong password!",
//                                 buttonText: "Okay",
//                                 buttonTextStyle: { color: "#008000" },
//                                 buttonStyle: { backgroundColor: "#5cb85c" }
//                             })}
//                     >
//                         <Text>Toast</Text>
//                     </Button>
//                 </Content>
//             </Container>
//         );
//     }
// }