import React, { Component } from 'react';
import { Conent, Item, Input, Icon, Button } from 'native-base';
export default class SearchBarExample extends Component {
    render() {
        return (
                <Conent searchBar rounded>
                    <Button transparent>
                        <Item>
                            <Icon name="ios-search" />
                            <Input placeholder="Search" />
                            <Icon name="ios-people" />
                        </Item>
                    </Button>
                </Conent>
        );
    }
}