import axios from 'axios';

import { apiUrl, apiKey, categoryNew } from "../config/constants";
import { cafesTypes, catalogTypes } from '../reducers/types';


export const getCafes = (params) => (dispatch)=> {
    dispatch({type: cafesTypes.LoaderStart});
    axios.get(`${apiUrl}/api/cafes`).then(response => {
        const payload = response.data.map(cafe => {
            cafe.image = cafe.pictures[0] != undefined
                ? {url: `${apiUrl}/${cafe.pictures[0].url.replace('.jpg', '-100x100.jpg')}`}
                : require('../themes/logo.png')
            return cafe;
        });
            dispatch({type: cafesTypes.ListReceive, payload})
        }).finally(()=>{
            dispatch({type: cafesTypes.LoaderEnd })
        // });
    });
};

export const setCafe = cafe => dispatch =>{
       dispatch ({type: cafesTypes.ItemReceive, payload: cafe });
};

export const getCafe = (params) => {
    const {id} = params;

    return axios.get(`${apiUrl}/api/cafes/${id}`)
        .then(response => {
            const cafe = response.data;

            cafe.image = cafe.pictures[0] != undefined
                ? {url: `${apiUrl}/${cafe.pictures[0].url}`}
                : require('../themes/logo.png')

            cafe.images = cafe.pictures.map(picture => {
                return {url: `${apiUrl}/${picture.url}`, id:picture.id}
            })
            return cafe;
        })
        .catch(error => {
            console.log(error);
        });
};
//
export const getCategories= (params) => (dispatch)=> {
    // const {category_id} = params;

    dispatch({type: catalogTypes.LoaderStart});
    axios.post('http://openapp.wsh.pp.ua/api', {
            'apiKey': `${apiKey}`,
            'getType': 'getCategories'
            // 'category_id': `${categoryNew}`
        }).then(response => {
            const payload = response.data.categories.map(item => {
                return item;
            });
                dispatch({type: catalogTypes.ListReceive, payload})
            }).finally(()=>{
                dispatch({type: catalogTypes.LoaderEnd })
            });

};
export const getChildCategories= (params) => (dispatch)=> {
    dispatch({type: catalogTypes.LoaderStart});
    axios.post('http://openapp.wsh.pp.ua/api', {
        'apiKey': `${apiKey}`,
        'getType': 'getCategories',
    }).then(response => {
        const payload = response.data.categories.map(item => {
            return item;
        });
        dispatch({type: catalogTypes.ListReceive, payload})
    }).finally(()=>{
        dispatch({type: catalogTypes.LoaderEnd })
    });

};
