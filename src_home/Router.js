import React from 'react';
import {Scene, Router, Stack} from 'react-native-router-flux';

import welcomeScene from './scenes/welcomeScene';
import catalogScene from './scenes/catalogScene';
import categoryScene from './scenes/categoryScene';

import singInScene from './scenes/signInScene';
import loginScene from './scenes/newLoginScene';
// import signUpScene from './scenes/signUpScene';

// import loginScene from './scenes/loginScene';
// import listScene from './scenes/listScene';
import itemScene from './scenes/itemScene';


const Routers = () => <Router>
  <Scene key="root">
    <Stack key="unregistered">
        <Scene key="signIn"
               component={singInScene}
               title=""
               hideNavBar
        />
        <Scene key="login"
               component={loginScene}
               title=""

               hideNavBar
        />
      <Scene key="welcome"
             component={welcomeScene}
             title=""
             initial
             hideNavBar
      />
      <Scene key="catalog"
             component={catalogScene}
             title=""
             hideNavBar
       />
        <Scene key="category"
               component={categoryScene}
               title=""

               hideNavBar
        />
      <Scene key="item"
             component={itemScene}
             title=""
             hideNavBar
      />

    </Stack>

  </Scene>
</Router>;


export default Routers;