import React, { Component } from 'react';
import { Container,Content, Tab, Tabs } from 'native-base';
import Tab1 from '../components/tabSigIn';
import Tab2 from '../components/tabSigUp';
import HeaderBar from '../components/header';
export default class Layout extends Component {

    render() {
        return(
             <Container >
                <HeaderBar title="Вход в магазин"/>
                <Content>
                    <Tabs>
                        <Tab
                            heading="Вход"
                            tabStyle={{backgroundColor: '#f8f8f8'}}
                            activeTabStyle={{backgroundColor: '#f8f8f8'}}
                            textStyle={{color: '#808080' }}
                            topTabBarBorderColor={{borderColor: '#e83c4d'}}
                            topTabBarActiveBorderColor={{borderColor: '#e83c4d'}}
                            activeTextStyle={{ color: '#e83c4d' }}
                            fontWeight="300"
                        >
                            <Tab1/>
                        </Tab>
                        <Tab heading="Регистрация"
                             tabStyle={{backgroundColor: '#f8f8f8'}}
                             activeTabStyle={{backgroundColor: '#f8f8f8'}}
                             textStyle={{color: '#808080' }}
                             topTabBarBorderColor={{borderColor: '#e83c4d'}}
                             topTabBarActiveBorderColor={{borderColor: '#e83c4d'}}
                             activeTextStyle={{ color: '#e83c4d' }}
                             fontWeight="300"
                        >
                            <Tab2/>
                        </Tab>
                    </Tabs>
                </Content>
            </Container>
        );
    }
}


