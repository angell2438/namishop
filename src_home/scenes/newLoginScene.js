import React, {Component} from 'react';
import {
    Header,
    Left,
    Container,
    Button,
    Body,
    Icon,
    Title,
    Tabs,
    Tab
} from 'native-base';
import { StatusBar } from 'react-native';
import {Actions} from 'react-native-router-flux';

import theme from '../themes/theme';
import styles from '../themes/style';
import LoginForm from '../components/loginForm';
import RegForm from '../components/regForm';

export default class loginScene extends Component {

    constructor () {
        super();
    }

    render () {
        return (

            <Container style={{backgroundColor: "#fff"}}>
                <Header androidStatusBarColor="#fff" hasTabs style={styles.header}>
                    <StatusBar barStyle="dark-content"/>
                    <Left>
                        <Button transparent>
                            <Icon android="md-menu" ios="ios-menu-outline" style={{
                                color: theme.colorTxt,
                                fontSize: 31
                            }}/>
                        </Button>
                    </Left>
                    <Body style={{flexGrow: 1}}>
                    <Title style={styles.title}>Вход в магазин</Title>
                    </Body>
                </Header>
                <Tabs initialPage={0}
                      tabBarUnderlineStyle={{
                          backgroundColor: theme.red
                      }}
                >
                    <Tab
                        heading="Вход"
                        tabStyle={{backgroundColor: '#fff'}}
                        activeTabStyle={{backgroundColor: '#fff'}}
                        textStyle={{color: theme.colorTxtDimmed }}
                        activeTextStyle={{ color: theme.red }}
                        fontWeight="300"
                    >
                        <LoginForm/>
                    </Tab>
                    <Tab heading="Регистрация"
                         tabStyle={{backgroundColor: '#fff'}}
                         activeTabStyle={{backgroundColor: '#fff'}}
                         textStyle={{color: theme.colorTxtDimmed }}
                         activeTextStyle={{ color: theme.red }}
                         fontWeight="300"
                    >
                        <RegForm/>
                    </Tab>

                </Tabs>
            </Container>

        );
    }
}