import React, { Component } from "react";
import { Container,Content, Text, Item } from 'native-base';
import ButtonItem from '../components/button_item';
import Product from "../components/itemProduct";
import HeaderBar from '../components/header';
import { productStyle, generalStyle } from "../themes/styles";
import Banner from "../components/banner";
import { Actions } from "react-native-router-flux";
// import Toaster from './src/components/toaster';
import SearchBarExample from "../components/search";

class welcomeScene extends Component {
  // constructor(){
  //   super();
  //   this.state = {
  //     isShow: true,
  //     icon: 'rocket'
  //   };
  // }
  //

  //
  // buttonClick() {
  //   this.setState(prevState=>{
  //     return { isShow: !prevState.isShow };
  //   })
  // }
  // toogleIcon(){
  //   this.setState(prevState=>{
  //     return { icon:
  //       prevState.icon=='rocket' ? 'user' : 'rocket'
  //     };
  //   })
  // }


  render(){
    return (
        <Container>
            <HeaderBar  title="Naomi Shop"/>
            {/*<SearchBarExample/>*/}
            <Content style={ generalStyle.bgColor }>
              <Banner/>
              <Item style= { productStyle.tabContainer } >
                  <ButtonItem text="Каталог товаров" click={ Actions.catalog }/>
              </Item>
                <Text style= { productStyle.titleStyle }>Новинки</Text>
              <Item style = { productStyle.productContainer }>
                  <Product title="Xiaomi Mi Max 24/64GB Pink" price='4537 грн'/>
                  <Product title="Xiaomi Mi Max 24/64GB Black" priceold='4000 грн' price='4537 грн' />
                  <Product title="Xiaomi Mi Max 24/64GB Pink" price='4537 грн'/>
                  <Product title="Xiaomi Mi Max 24/64GB Black" priceold='4000 грн' price='4537 грн' />
                  <Product title="Xiaomi Mi Max 24/64GB Pink" price='4537 грн'/>
                  <Product title="Xiaomi Mi Max 24/64GB Black" priceold='4000 грн' price='4537 грн' />
              </Item>
                <Text style= { productStyle.titleStyle }>Популярні</Text>
              <Item style = { productStyle.productContainer }>
                  <Product title="Xiaomi Mi Max 24/64GB Pink" price='4537 грн'/>
                  <Product title="Xiaomi Mi Max 24/64GB Black" priceold='4000 грн' price='4537 грн' />
                  <Product title="Xiaomi Mi Max 24/64GB Pink" price='4537 грн'/>
                  <Product title="Xiaomi Mi Max 24/64GB Black" priceold='4000 грн' price='4537 грн' />
                  <Product title="Xiaomi Mi Max 24/64GB Pink" price='4537 грн'/>
                  <Product title="Xiaomi Mi Max 24/64GB Black" priceold='4000 грн' price='4537 грн' />
              </Item>
            </Content>
        </Container>
    );
  }
}

export default welcomeScene;