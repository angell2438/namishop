import {View, Text, Image} from "react-native";
import React, {Component} from "react";
import { Container,Content,Icon, Item } from 'native-base';
import HeaderBar from '../components/header';
import { getCafe } from '../actions/categoryActions';
import Loader from '../components/loader';
import {cafeItemStyle, generalStyle} from "../themes/styles";

class itemScene extends Component {

    constructor() {
        super();
        this.state = {cafe: {}, loading: false}
    }

    componentWillMount() {
        this.setState({loading: true});

        getCafe({id: this.props.id})
            .then(cafe => {
                this.setState({cafe});
            })
            .finally(() => {
                this.setState({loading: false});
            })
    }

    renderRating(cafe) {
        const rating = [];
        for (let i = 0; i < cafe.rating; i++) {
            rating.push(<Icon key={5 - i} name="star" size={20} color="#f39c12"/>)
        }
        for (let i = 0; i < 5 - cafe.rating; i++) {
            rating.push(<Icon key={5 - i} name="star-o" size={20} color="#f39c12"/>)
        }
        return rating;
    }

    renderCafe() {
        const {cafe} = this.state;
        return <Content style={ generalStyle.bgColor }>
            <Image style={cafeItemStyle.mainImage} source={cafe.image}/>
            <Item style={cafeItemStyle.titleLine}>
                <Text style={cafeItemStyle.title}>{cafe.name}</Text>
                <Item style={cafeItemStyle.rating}>{this.renderRating(cafe)}</Item>
            </Item>
        </Content>
    }

    render() {
        return (
            <Container>
                <HeaderBar/>
                {/*<SearchBarExample/>*/}
                {this.state.loading ? <Loader/> : this.renderCafe()}
            </Container>
        )
    }
}

export default itemScene;