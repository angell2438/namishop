import {Container, Header, Content, List, ListItem, Text} from 'native-base';
import React, {Component} from "react";
import {Actions} from 'react-native-router-flux';
import HeaderBar from '../components/header';
import {getCategorys} from "../actions/categoryActions";
import { generalStyle } from "../themes/styles";

class listScene extends Component {
    constructor() {
        super();
        this.state = {cafes: []}
    }

    componentWillMount() {
        getCategorys().then(response => {
            return response.data;
        }).then(cafes => {
            this.setState({cafes})
        })
            .catch(error => {
                console.log(error)
            })
    }

    itemPress(index) {
        Actions.item({index})
    }

    renderCategorys() {
        return this.state.cafes.map(cafe => (
            {/*<TouchableOpacity key={cafe.id}>*/}
        {/*<Text>{cafe.name}</Text>*/
        }
        {/*<Text>{cafe.description}</Text>*/
        }
        {/*</TouchableOpacity>*/
        }
        <ListItem key={cafe.id}>
            <Text>{cafe.name}</Text>
            <Text>{cafe.description}</Text>
        </ListItem>
    )
    )
        ;
    }

    render() {
        return (
            <Container>
                <HeaderBar/>
                <Content style={generalStyle.container}>
                    {this.renderCategorys()}
                </Content>
            </Container>
        )
    }
}

export default listScene;