import { Container, Content, List, ListItem, Right, Icon,  Body, Text } from 'native-base';
import React, {Component} from "react";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Loader from "../components/loader";
import {cafeListStyle, generalStyle, categoryStyle} from "../themes/styles";
import { getChildCategories } from "../actions/categoryActions";
import HeaderBar from "../components/header";
import category from "../reducers/categoryReducer";
class categoryScene extends Component {

    componentWillMount() {
        this.props.getChildCategories();
    }

    itemPress(category_id) {
        Actions.item({category_id})
    }

    renderCafes() {
        // const {child} = this.props.category;
        return this.props.category.byIds.map(itemCategory => {
            // const cafe = this.props.category.category.byIds
            return(
        <ListItem avatar onPress={this.itemPress.bind(this, itemCategory)} style={cafeListStyle.cafeWrap} key={itemCategory.category_id}>
            <Body>
                 <Text style={cafeListStyle.title}>{itemCategory.name}</Text>
            </Body>
            <Right>
                <Icon name="angle-right" style={categoryStyle.iconArrow}/>
            </Right>
        </ListItem>
            // const {name} = itemCategory.name;
            )
    }
        );
    }

    render() {
        return (
            <Container>
                <HeaderBar title="name"/>
                <Content style={generalStyle.container}>
                    <List>
                        {this.props.category.loading ? <Loader/> : this.renderCafes()}
                    </List>
                </Content>
            </Container>
        )
    }
}
const  mapStateToProps = ({category})=>{
    return { category }
};
const  mapActionsToProps = {
    getChildCategories
};
export default connect( mapStateToProps, mapActionsToProps )(categoryScene);
