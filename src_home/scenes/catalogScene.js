import { Container,Content, List, ListItem, Left, Icon, Body, Text } from 'native-base';
import React, { Component } from "react";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Loader from "../components/loader";
import HeaderBar from '../components/header';
import { getCategories } from "../actions/categoryActions";
import { generalStyle, cafeListStyle } from "../themes/styles";
class catalogScene extends Component {

    componentWillMount() {
        this.props.getCategories();
    }

    itemPress() {
        // this.props.setCafe(cafe);
        Actions.category({item})
    }
    // itemPress(category_id) {
    //     Actions.category({category_id})
    // }
    renderCategories(){
        return this.props.catalog.list.map(item =>(
            <ListItem avatar style={cafeListStyle.cafeWrap}  key={item.category_id}>
                <Left>
                    <Icon name="plane" color="#000"/>
                </Left>
                <Body>
                <Text style={ generalStyle.linkInside }>{item.name}</Text>
                </Body>
            </ListItem>
            )
        );
    }

    render(){
        return (
            <Container>
                <HeaderBar title="Каталог товаров"/>
                <Content style={ generalStyle.container }>
                    <List>
                        {this.props.catalog.loading ? <Loader/> : this.renderCategories()}
                    </List>
                </Content>
            </Container>
        );
    }
}
const  mapStateToProps = ({catalog})=>{
    return { catalog }
};
const  mapDispatchToProps = {
    getCategories
};
export default connect(mapStateToProps, mapDispatchToProps) (catalogScene);

