import { Text } from "react-native";
import React from 'react';
import styles from '../themes/style';

const Loader = (props)=>(
  <Text style={styles.txt } >Loading...</Text>
);

export default Loader;