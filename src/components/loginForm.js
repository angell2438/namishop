import React from 'react';
import {
    Body,
    Button,
    CheckBox,
    Col,
    Container,
    Content,
    Grid,
    H1,
    Input,
    Item,
    ListItem,
    Row,
    Spinner,
    Text
} from 'native-base';

import MyIcon from '../icons/customIcon';

import {connect} from 'react-redux';

import Image from 'react-native-remote-svg';
import {Actions} from 'react-native-router-flux';
import {signIn, changeAuthData} from '../actions/myActions';


import theme from '../themes/theme';
import styles from '../themes/style';

class LoginForm extends React.Component {

    constructor () {
        super();

        this.state = {
            checked: false,
            loading: false
        }
    }

    componentDidMount() {
        console.log(this.props.auth)
    }

    check() {
        this.setState({
            checked: !this.state.checked
        })
    }

    logIn () {
        this.setState({loading: true});

        this.props.signIn(this.props.auth)
            .then(Actions.main)
            .catch(error => alert(error))
            .finally(()=> {
                this.setState({loading: false});
            })
    }

    renderForm () {
        return (
            <Content padder>

                <H1 style={ styles.sectionHeader }>Вход</H1>
                <Item style={{ borderBottomWidth: 0 }}>
                    <MyIcon active name='envelope-of-white-paper'
                          style={{
                              color: theme.red,
                              fontSize: 21,
                              width: 30
                          }}
                    />
                    <Input placeholder='Введите телефон или email'
                           placeholderTextColor={theme.colorTxtDimmed}
                           style={styles.input}
                           underlineColorAndroid='transparent'
                           onChangeText={ (email) => this.props.changeAuthData('email', email) }
                           value={this.props.auth.email}
                           keyboardType="email-address"
                           autoCapitalize="none"
                           autoCorrect={false}
                    />
                </Item>
                <Item style={{ borderBottomWidth: 0 }}>
                    <MyIcon active name='lock'
                          style={{
                              color: theme.red,
                              fontSize: 26,
                              width: 30,
                          }}
                    />
                    <Input placeholder='Пароль'
                           placeholderTextColor={ theme.colorTxtDimmed }
                           style={ styles.input }
                           secureTextEntry={ !this.state.checked }
                           underlineColorAndroid='transparent'
                           onChangeText={ (password) => this.props.changeAuthData('password', password) }
                           value={this.props.auth.password}
                    />
                </Item>

                <Grid>
                    <Row style={{ alignItems: 'center' }}>
                        <Col>
                            <ListItem style={{
                                borderBottomWidth: 0,
                                paddingLeft: 5,
                                marginLeft: 0,
                                paddingRight: 0
                            }}>
                                <CheckBox color={theme.red}
                                          checked={ this.state.checked }
                                          onPress={ this.check.bind(this) }
                                />
                                <Body>
                                    <Text style={ styles.txt }>
                                        Показать пароль
                                    </Text>
                                </Body>
                            </ListItem>
                        </Col>          
                        <Col>
                            <Button transparent style={{ marginLeft: 'auto' }}>
                                <Text uppercase={false} 
                                      style={{ color: theme.red, textAlign: 'right', fontSize: theme.fontSize, fontFamily: 'Geometria-Medium' }}>
                                    Забыли пароль?
                                </Text>
                            </Button>
                        </Col>
                    </Row>
                </Grid>

                <Button block danger title="Войти" style={{
                    backgroundColor: theme.red,
                    borderRadius: 5,
                    marginTop: 30,
                    marginBottom: 30
                }} onPress={ this.logIn.bind(this) }>
                    <Text uppercase={false} style={ styles.btnTxt }>Войти</Text>
                </Button>

                <Row>
                    <Text style={ [styles.txt, {flex: 1, textAlign: 'center'}] }>Войти через соцсети</Text>
                </Row>
                <Row style={{justifyContent: 'space-evenly',marginTop: 10}}>

                    <Button title="facebook" transparent iconLeft>
                        <Image
                            style={{
                                width: 17,
                                height: 30
                            }}
                            source={require('../img/fb.svg')}
                        />
                        <Text uppercase={false} style={{fontSize: theme.fontSize + 2, fontFamily: 'Geometria-Medium'}}>Facebook</Text>
                    </Button>

                    <Button title="Google Plus" transparent iconLeft>
                        <Image
                            style={{
                                width: 41,
                                height: 27
                            }}
                            source={require('../img/google-plus.svg')}
                        />
                        <Text style={{color: theme.red, fontSize: theme.fontSize + 2, fontFamily: 'Geometria-Medium'}} uppercase={false}>Google+</Text>
                    </Button>

                </Row>
            </Content>
        )
    }

    render() {
        return (

            <Container style={{ justifyContent: 'center' }}>
                {
                    this.state.loading ? <Spinner color={theme.red} /> : this.renderForm()
                }
            </Container>
        )
    }
}

const mapStateToProps = ({auth}) => {
    return {auth}
};

export default connect(mapStateToProps, {signIn, changeAuthData})(LoginForm);