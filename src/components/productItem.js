import React, {Component} from 'react';
import {Button, Col, Grid, H2, Text, View} from "native-base";
import {StyleSheet} from 'react-native';
import Image from 'react-native-remote-svg';

import theme from '../themes/theme';
import styles from '../themes/style';

export default class ProductItem extends Component {
    render() {
        return (
            <View style={product.wrapper}>
                <View style={product.img}>
                    {
                        this.props.badge ?
                        <View style={product.badge}>
                            <Text style={product.badgeTxt}>Топ продаж</Text>
                        </View>
                        : null
                    }
                    <Image source={ require('../img/product.jpg') } style={{width: 75, height: 90}}/>
                </View>
                <H2 style={styles.txt }>{this.props.title || 'Product Title'}</H2>
                <Grid style={product.footer}>
                    <Col size={2}>
                        { this.props.oldPrice ? <Text style={product.oldPrice}>{`${this.props.oldPrice} грн`}</Text> : null }
                        { this.props.price ? <Text style={product.price}>{`${this.props.price} грн`}</Text> : null }
                    </Col>
                    <Col size={1}>
                        <Button full transparent style={{padding: 0}}>
                            <Image source={ require('../img/more.svg')}/>
                        </Button>
                    </Col>
                </Grid>
            </View>
        );
    }
}

const product = StyleSheet.create({
    wrapper: {
        padding: 15,
        paddingBottom: 0,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#e6e6e6',
        flex: 1
    },
    badge: {
        padding: 6,
        paddingHorizontal: 10,
        backgroundColor: '#f58301',
        borderRadius: 15,     
        minWidth: 65,
        position: 'absolute',
        top: -8,
        right: '35%',
        zIndex: 10,
        elevation: 1
    },
    badgeTxt: {
        color: '#fff',
        fontSize: theme.fontSize - 4,
        lineHeight: theme.fontSize - 4,
        fontFamily: 'Geometria-Medium',
    },
    img: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 15,
        marginTop: 5
    },
    price: { 
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
        fontFamily: 'Geometria-Bold',
    },
    oldPrice: {
        fontFamily: 'Geometria',
        fontSize: theme.fontSize - 4,
        lineHeight: theme.fontSize - 4,
        color: '#808080',
        textDecorationLine: 'line-through',
        position: 'absolute',
        top: -theme.fontSize,
        left: 0,
        zIndex: 5 
    },
    footer: {
        alignItems: 'center',
        marginRight: -15,
        marginTop: 5
    }
});