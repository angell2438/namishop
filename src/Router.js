import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';

import loginScene from './scenes/loginScene';
import mainScene from './scenes/mainScene';
import catalogScene from './scenes/productsCatalog';


export default class Hello extends Component {
    render () {
        return (
            <Router>
                <Scene key="root">
                    <Scene
                        key="login"
                        component={loginScene}
                        hideNavBar
                    />
                    <Scene
                        key="main"
                        component={mainScene}
                        hideNavBar
                        direction="horizontal"
                        initial
                    />
                    <Scene key="catalog"
                           component={catalogScene}
                           hideNavBar
                           direction="horizontal"
                    />
                </Scene>
            </Router>
        )
    }
}