import {StyleSheet} from 'react-native';
import theme from './theme';

const styles = StyleSheet.create({
    title: {
        color: theme.colorTxt,
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
        fontFamily: 'Geometria-Medium',
    },
    sectionHeader: {
        textAlign: 'center',
        marginTop: 30,
        marginBottom: 20,
        fontFamily: 'Geometria-Medium',
        fontSize: theme.fontSize + 8,
        lineHeight: theme.fontSize + 8,
        color: theme.colorTxt
    },
    tabStyle: {
        backgroundColor: '#fff'
    },
    tabTextStyle: {
        color: theme.colorTxtDimmed,
        fontFamily: 'Geometria'
    },
    tabActiveTextStyle: {
        color: theme.red,
        fontFamily: 'Geometria',
        fontWeight: '400'
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#e6e6e6',
        fontSize: theme.fontSize,
        fontFamily: 'Geometria'
    },
    txt: {
        color: theme.colorTxt,
        fontSize: theme.fontSize,
        lineHeight: theme.fontSize,
        fontFamily: 'Geometria'
    },
    txtLarge: {
        color: theme.colorTxt,
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
        fontFamily: 'Geometria'
    },
    btnTxt: {
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
        fontFamily: 'Geometria-Medium'
    },
    header: {
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: "#e6e6e6"
    },
    searchBar: {
        backgroundColor: "rgba(0, 0, 0, .05)",
        borderRadius: 5,
        marginBottom: 10
    },
    slide: {
        backgroundColor: '#f1f1f1',
        flex: 1
    },
    image: {
        flex: 1,
        width: '100%'
    }
});

export default styles;
