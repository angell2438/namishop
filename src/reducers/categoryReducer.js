import { cafesTypes } from './types';

const NormalizeCafeList = (cafes)=> {
    let normalizedReviews={
        byIds:{},
        allIds:[]
    }
    let normalizeCafes={
        byIds:cafes.reduce((prev, current)=>{
            prev[current.id]= current;
            if(current.reviews)
            current.reviews.foreach(review=>{
                normalizedReviews.byIds[review.id] = review;
                normalizedReviews.allIds.push(review.id);
            });

            return prev;
    },{}),
        allIds:cafes.map((current)=> current.id)
    }
    normalizedReviews.allIds =
        Object.keys(normalizedReviews.byIds);
    return {...state, cafes: normalizeCafes, reviews:normalizedReviews, loading: false}
}

const INITIAL_STATE = {
    cafes:{
      byIds:{},
      allIds:[]
    },
    reviews:{
        byIds:{},
        allIds:[]
    },
    list: [],
    current: {},
    loading: false
};

export default function reducer(state = INITIAL_STATE, action) {
    // console.log(action);

    switch (action.type) {
        case cafesTypes.LoaderStart:
            return { ...state, loading: true };
        case cafesTypes.LoaderEnd:
            return { ...state, loading: false };
        case cafesTypes.ListReceive:
            return NormalizeCafeList(state, action.payload);
             // return { ...state, list: action.payload, loading: false};
        case cafesTypes.ListReceive:
            return { ...state, current: action.payload };
        default:
            return state;
    }
}
