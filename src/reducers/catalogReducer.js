import { catalogTypes } from './types';

const INITIAL_STATE = {
    list: [],
    current: {},
    loading: false
};

export default function reducer(state = INITIAL_STATE, action) {
    // console.log(action);

    switch (action.type) {
        case catalogTypes.LoaderStart:
            return { ...state, loading: true };
        case catalogTypes.LoaderEnd:
            return { ...state, loading: false };
        case catalogTypes.ListReceive:
             return { ...state, list: action.payload, loading: false};
        case catalogTypes.ItemReceive:
            return { ...state, current: action.payload };
        default:
            return state;
    }
}
