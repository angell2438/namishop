import axios from 'axios';
import firebase from '../config/firebase';

export const apiUrl = 'http://coffee-points.dev2.devloop.pro';

export const getCafes = () => {
    return axios.get(apiUrl + '/api/cafes').then(response => {
        return response.data
    });
};

export const changeAuthData = (field, value) => (dispatch) => {
    dispatch({
        type: 'auth_change_data',
        payload: {field, value}
    });

};

export const signIn = ({email, password}) => (dispatch) => {

    if (email === "") {
        return Promise.reject('enter email')
    }
    if (password === "") {
        return Promise.reject('enter password')
    }

    return firebase.auth()
        .signInWithEmailAndPassword(email, password).then(() => {
            const curUser = firebase.auth().currentUser;
            const ref = firebase.database().ref(`users/${curUser.uid}`);

            ref.once('value').then(function (snapshot) {
                dispatch({type: 'auth_user_receive', payload: snapshot.val()});
            });

        });
};

export const signUp = ({name, email, password}) => (dispatch) => {

    return firebase.auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
            const curUser = firebase.auth().currentUser;
            return curUser
                .sendEmailVerification({})
                .then(()=>{
                    firebase.database().ref(`users/${curUser.uid}`)
                        .set({name, email, password});
                });
        }).catch(error => Promise.reject(error))
};