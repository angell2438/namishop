import axios from 'axios';

import { apiUrl, apiKey } from "../config/constants";
import { cafesTypes, catalogTypes } from '../reducers/types';

export const getCategories= (params) => (dispatch)=> {
    dispatch({type: catalogTypes.LoaderStart});
    axios.post('http://openapp.wsh.pp.ua/api', {
            'apiKey': `${apiKey}`,
            'getType': 'getCategories',
        }).then(response => {
            const payload = response.data.categories.map(item => {
                return item;
            });
                dispatch({type: catalogTypes.ListReceive, payload})
            }).finally(()=>{
                dispatch({type: catalogTypes.LoaderEnd })
            });

};
export const getChildCategories= (params) => (dispatch)=> {
    dispatch({type: catalogTypes.LoaderStart});
    axios.post('http://openapp.wsh.pp.ua/api', {
        'apiKey': `${apiKey}`,
        'getType': 'getCategories',
    }).then(response => {
        const payload = response.data.categories.map(item => {
            return item;
        });
        dispatch({type: catalogTypes.ListReceive, payload})
    }).finally(()=>{
        dispatch({type: catalogTypes.LoaderEnd })
    });

};
