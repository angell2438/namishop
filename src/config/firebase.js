import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBTTjOapXt_o_V5Ln3qJ5y4AvAURBsjHoc",
    authDomain: "test-a7ceb.firebaseapp.com",
    databaseURL: "https://test-a7ceb.firebaseio.com",
    projectId: "test-a7ceb",
    storageBucket: "test-a7ceb.appspot.com",
    messagingSenderId: "555270594713"
};
firebase.initializeApp(config);

export default firebase;
