import React, {Component} from 'react';
import {
    Badge,
    Body,
    Button,
    Container,
    Content,
    Header,
    Icon,
    Input,
    Item,
    Left,
    List,
    ListItem,
    Right,
    Text,
    Title
} from "native-base";
import Image from 'react-native-remote-svg';
import {StatusBar} from "react-native";
import { connect } from 'react-redux';
import { getCategories } from "../actions/categoryActions";
import Loader from "../components/loader";
import MyIcon from '../icons/customIcon';
import theme from "../themes/theme";
import styles from "../themes/style";
import {Actions} from "react-native-router-flux";

class Catalog extends Component {
    componentWillMount() {
        this.props.getCategories();
    }
    itemPress() {
        // this.props.setCafe(cafe);
        Actions.category({item})
    }
    state = {
        data: [
            {
                // name: 'Ноутбуки и компьютеры',
                icon: 'laptop'
            },
            {
                // name: 'Смартфоны, ТВ и электроника',
                icon: 'smartphone-2'
            },
            {
                // name: 'Бытовая техника',
                icon: 'vacuum-cleaner'
            },
            {
                // name: 'Товары для дома',
                icon: 'bedside-table'
            },
            {
                // name: 'Инструменты и автотовары',
                icon: 'wheel'
            },
            {
                // name: 'Сантехника и ремонт',
                icon: 'wrench'
            },
            {
                // name: 'Дача, сад и огород',
                icon: 'sand-bucket'
            },
            {
                // name: 'Спорт и увлечения',
                icon: 'football'
            },
        ]
    };
    renderCategories(){
        return this.props.catalog.list.map(item =>(
            <ListItem icon key={item.category_id} style={{height: 56}} onPress={() => console.log(item.category_id)}>
                <Left>
                    <MyIcon name={item.icon} size={32} color={theme.red}/>
                </Left>
                <Body style={{height: 56}} pointerEvents='none'>
                <Text style={[styles.txt, {color: theme.colorTxtDimmed, fontSize: 15}]}>{item.name}</Text>
                </Body>
            </ListItem>
            )
        );
    }
    render() {
        return (
            <Container style={{ backgroundColor: '#fff'}}>
                <Header hasTabs style={[styles.header, { backgroundColor: theme.backgroundColorDimmed }]}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent>
                            <Icon android="md-menu" ios="ios-menu-outline" style={{
                                color: theme.colorTxt,
                                fontSize: 31
                            }}/>
                        </Button>
                    </Left>
                    <Body style={{ flex: 2, alignItems: 'center' }}>
                        <Title style={styles.title}>Каталог товаров</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent>
                            <Badge transform style={{
                                position: 'absolute',
                                zIndex: 10,
                                right: 3,
                                top: 1,
                                backgroundColor: theme.red,
                                transform: [{scaleX: .7}, {scaleY: .7}],
                            }}>
                                <Text style={{fontWeight: '600'}}>1</Text>
                            </Badge>
                            <Image style={{
                                width: 21,
                                height: 20
                            }}
                                   source={require('../img/basket.svg')}/>
                        </Button>

                    </Right>
                </Header>
                <Header searchBar hasTabs androidStatusBarColor={theme.backgroundColorDimmed}
                        style={[styles.header, {marginTop: -5, backgroundColor: theme.backgroundColorDimmed}]}>
                    <StatusBar barStyle="dark-content"/>
                    <Item style={ styles.searchBar }>
                        <Icon name="ios-search" style={{ color: theme.colorTxtDimmed }} />
                        <Input placeholder="Я ищу..."
                               placeholderTextColor={theme.colorTxtDimmed}
                               autoCapitalize="none"
                               style={styles.txt}
                        />
                    </Item>

                </Header>

                <Content>
                    <List dataArray={this.state.data} >
                          {this.props.catalog.loading ? <Loader/> : this.renderCategories()}
                    </List>
                </Content>

            </Container>
        );
    }
}

const  mapStateToProps = ({catalog})=>{
    return { catalog }
};
const  mapDispatchToProps = {
    getCategories
};
export default connect(mapStateToProps, mapDispatchToProps) (Catalog);