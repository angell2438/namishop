import React from 'react';
import {
    Badge,
    Body,
    Button,
    Col,
    Container,
    Content,
    Grid,
    Header,
    Icon,
    Input,
    Item,
    Left,
    ListItem,
    Right,
    Row,
    Text,
    Title
} from 'native-base';

import {StatusBar, View} from 'react-native';
import Slick from 'react-native-slick';
import Image from 'react-native-remote-svg';
import { Actions } from 'react-native-router-flux';

import theme from '../themes/theme';
import styles from '../themes/style';
import {getCafes} from '../actions/myActions';
import ProductItem from "../components/productItem";

import MyIcon from '../icons/customIcon';


export default class MainScene extends React.Component {

    constructor () {
        super();
        this.state = {
            cafes: [],
            loading: false
        }
    }

    componentDidMount() {

        this.setState({ loading: true }, () => {
            getCafes()
                .then(cafes => {
                    this.setState({
                        loading: false,
                        cafes
                    });
                })
        });
    }

    renderItems () {
        return this.state.cafes.map((cafe, i) => (

            <ListItem avatar key={i}>
                <Left>
                    {/*<Thumbnail source={{uri: cafe.owner.picture ? apiUrl + cafe.owner.picture : ''}}/>*/}
                </Left>
                <Body>
                    <Text>{ cafe.name }</Text>
                    <Text note>{ cafe.description }</Text>
                </Body>
                <Right>
                    {/*<Text> { console.table(cafe) } </Text>*/}
                </Right>
            </ListItem>

        ));
    }

    render() {
        return (
            <Container style={{backgroundColor: "#fff"}}>
                <Header hasTabs style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent>
                            <Icon android="md-menu" ios="ios-menu-outline" style={{
                                color: theme.colorTxt,
                                fontSize: 31
                            }}/>
                        </Button>
                    </Left>
                    <Body style={{ flex: 1, alignItems: 'center' }}>
                        <Title style={styles.title}>Naomi Shop</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent>
                            <Badge transform style={{
                                position: 'absolute',
                                zIndex: 10,
                                right: 3,
                                top: 1,
                                backgroundColor: theme.red,
                                transform: [{scaleX: .7}, {scaleY: .7}],
                            }}>
                                <Text style={{fontWeight: '600'}}>1</Text>
                            </Badge>
                            <Image style={{
                                width: 21,
                                height: 20
                            }}
                                   source={require('../img/basket.svg')}/>
                        </Button>

                    </Right>
                </Header>
                <Header searchBar hasTabs androidStatusBarColor="#fff" style={[styles.header, {marginTop: -5}]}>
                    <StatusBar barStyle="dark-content"/>
                    <Item style={ styles.searchBar }>
                        <Icon name="ios-search" style={{ color: theme.colorTxtDimmed }} />
                        <Input placeholder="Я ищу..."
                               placeholderTextColor={theme.colorTxtDimmed}
                               autoCapitalize="none"
                               style={styles.txt}
                        />
                    </Item>

                </Header>
                <Content>
                    <Slick style={styles.wrapper}
                           height={120}
                           dotStyle={{
                               borderColor: theme.colorTxt,
                               borderWidth: 1
                           }}
                           activeDotStyle={{
                               borderWidth: 0
                           }}
                           dotColor="transparent"
                           activeDotColor={theme.red}
                           paginationStyle={{
                               left: '85%',
                               bottom: 10
                           }}
                           loop={false}
                    >
                        <View style={styles.slide}>
                            <Image style={styles.image} source={require('../img/slider.jpg')} />
                        </View>
                        <View style={styles.slide}>
                            <Image style={styles.image} source={require('../img/slider.jpg')} />
                        </View>
                        <View style={styles.slide}>
                            <Image style={styles.image} source={require('../img/slider.jpg')} />
                        </View>
                    </Slick>

                    <Button full title="Каталог товаров" style={{
                        backgroundColor: theme.red,
                        borderRadius: 5,
                        margin: 15,
                        marginBottom: 0
                    }}
                            onPress={()=> Actions.catalog()}
                    >
                        <Text uppercase={false} style={ styles.btnTxt }>Каталог товаров</Text>
                    </Button>

                    <Grid>
                        <Row style={{ marginHorizontal: 15, marginBottom: 10, marginTop: 15}}>
                            <Text style={styles.txt}>Новинки</Text>
                        </Row>
                        <Row>
                            <Col>
                                <ProductItem image={'../img/product.jpg'}
                                             badge={true}
                                             title={'Xiaomi Redmi 4X 3/32GB Pink'}
                                             price={'4 099'} />
                            </Col>
                            <Col style={{ marginLeft: -1 }}>
                                <ProductItem image={'../img/product.jpg'}
                                             title={'Xiaomi Mi Max 2 4/64GB Black'}
                                             oldPrice={'6 099'}
                                             price={'4 099'} />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <ProductItem image={'../img/product.jpg'}
                                             badge={true}
                                             title={'Xiaomi Redmi 4X 3/32GB Pink'}
                                             price={'4 099'} />
                            </Col>
                            <Col style={{ marginLeft: -1 }}>
                                <ProductItem image={'../img/product.jpg'}
                                             title={'Xiaomi Mi Max 2 4/64GB Black'}
                                             oldPrice={'6 099'}
                                             price={'4 099'} />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <ProductItem image={'../img/product.jpg'}
                                             title={'Xiaomi Redmi 4X 3/32GB Pink'}
                                             price={'4 099'} />
                            </Col>
                            <Col style={{ marginLeft: -1 }}>
                                <ProductItem image={'../img/product.jpg'}
                                             badge={true}
                                             title={'Xiaomi Mi Max 2 4/64GB Black'}
                                             oldPrice={'6 099'}
                                             price={'4 099'} />
                            </Col>
                        </Row>
                    </Grid>

                    <Grid>
                        <Row style={{ marginHorizontal: 15, marginBottom: 10, marginTop: 15}}>
                            <Text style={styles.txt}>Популярные</Text>
                        </Row>
                        <Row>
                            <Col>
                                <ProductItem image={'../img/product.jpg'}
                                             title={'Xiaomi Redmi 4X 3/32GB Pink'}
                                             price={'4 099'} />
                            </Col>
                            <Col style={{ marginLeft: -1 }}>
                                <ProductItem image={'../img/product.jpg'}
                                             badge={true}
                                             title={'Xiaomi Mi Max 2 4/64GB Black'}
                                             oldPrice={'6 099'}
                                             price={'4 099'} />
                            </Col>
                        </Row>
                    </Grid>
                    
                </Content>
            </Container>
        );
    }
}