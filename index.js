import {AppRegistry} from 'react-native';
import React from 'react';
import Router from './src/Router';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from "redux";
// import App from './src/scenes/loginScene';
import ReduxThunk from 'redux-thunk';

import reducers from './src/reducers';

console.ignoredYellowBox = [
    'Warning: componentWillMount is deprecated',
    'Warning: componentWillUpdate is deprecated',
    'Warning: componentWillReceiveProps is deprecated',
    'Warning: isMounted(...) is deprecated',
];

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

const App = () =>
    <Provider store={store}>
        <Router/>
    </Provider>;



AppRegistry.registerComponent('projectReact', () => App);
